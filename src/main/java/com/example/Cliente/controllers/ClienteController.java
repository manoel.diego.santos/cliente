package com.example.Cliente.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.Cliente.models.Cliente;
import com.example.Cliente.services.ClienteService;

@RestController
@RequestMapping("/cliente")
public class ClienteController {
	
	@Autowired
	ClienteService clienteService;
	
	@PostMapping
	public void inserirCliente(@RequestBody Cliente cliente) {
		clienteService.incluirCliente(cliente);
	}
	
	@GetMapping("/{id}")	
	public Cliente consultarPorID(@PathVariable int id) {
		
		return clienteService.consultaClienteID(id).get();
	}

}
