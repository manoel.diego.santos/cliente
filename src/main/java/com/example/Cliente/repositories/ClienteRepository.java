package com.example.Cliente.repositories;

import org.springframework.data.repository.CrudRepository;

import com.example.Cliente.models.Cliente;

public interface ClienteRepository extends CrudRepository<Cliente, Integer> {

}
