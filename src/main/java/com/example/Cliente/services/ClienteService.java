package com.example.Cliente.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.Cliente.models.Cliente;
import com.example.Cliente.repositories.ClienteRepository;

@Service
public class ClienteService {
	
	@Autowired
	ClienteRepository clienteRepository;
	
	public void incluirCliente(Cliente cliente) {
		
		clienteRepository.save(cliente);
	}
	
	public Optional<Cliente> consultaClienteID(int id) {
		
		return clienteRepository.findById(id);
	}

}
